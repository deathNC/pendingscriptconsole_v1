/*
 * Neiro Technology 2011-2013
 * 
 * Pending Script Package class
 * 
 * Пакет может содержать исключительно только текстовую информацию, что в
 * интересах кроссплатформенности.
 * Один элемент внутри пакета представляет из себя запись с двумя полями: id и value.
 * Манипуляция значениями производится через текстовый идентификатор.
 * Количество элементов в пакете не ограничено.
 */
package textsockets;

import java.util.ArrayList;
import java.util.List;

/**
 * Пакет данных для сетевой системы Pending Script Console
 * @author deathNC
 */
public class PSPackage {
    
    /**
     * Тестовая функия
     */
    public static void main(String[] args) {
        long tm1 = System.currentTimeMillis();
        for (int i = 0; i < 1000000; ++i) {
            PSPackage pkg = new PSPackage();
            pkg.set("cmd", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p1", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p2", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p3", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p4", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p5", Long.toHexString(System.currentTimeMillis()));
            pkg.set("p6", Long.toHexString(System.currentTimeMillis()));
            StringBuilder sb = new StringBuilder(pkg.pack());
            pkg.unpack(sb);
        }
        long tm2 = System.currentTimeMillis();
        System.out.println("time: " + (tm2 - tm1));
    }
    
    public PSPackage() {
        items = new ArrayList<>();
    }

    /**
     * Создаёт пакет сразу с командой
     * @param cmd команда "cmd"
     */
    public PSPackage(String cmd) {
        this.items = new ArrayList<>();
        this.set("cmd", cmd);
    }
    
    
    
    /**
     * Добавить новый элемент или заменить значение уже имеющегося (на совпадении id)
     * @param id идентификатор элемента
     * @param value значение элемента - новое или замена предыдущему
     */
    public void set(String id, String value) {
        int hash = id.hashCode();
        for (Item i : items)
            if (i.hash == hash) {
                i.value = value;
                return;
            }
        items.add(new Item(id, value));
    }
    
    /**
     * Получить значение элемента по идентификатору
     * @param id искомый идентификатор
     * @return null еслиничего не найдено
     */
    public String get(String id) {
        int hash = id.hashCode();
        for (Item i : items)
            if (hash == i.hash) return i.value;
        return null;
    }
    
    /**
     * Сборка пакета в строку вида: hash;id1=value1;...;idN=valueN, окаймлённую
     * треугольными скобками
     * @return пакет, заключённый в строку
     */
    public String pack() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        for (Item i : items) {
            sb.append(i.id);
            sb.append('=');
            sb.append(strEncode(i.value));
            sb.append(';');
        }
        sb.append(">");
        sb.insert(1, (sb.length() - 2) + ";");
        return sb.toString();
    }
    
    /**
     * Распаковывает пакет из строки
     * @param sb накопитель StringBuilder. Первые найденные ограничители (треугольные скобки)
     *      будут удалены из этого накопителя (принцип стека)
     * @return true, если пакет был успешно собран из части строки
     */
    public boolean unpack(StringBuilder sb) {
        items.clear();
        // -- парсим границы пакета в строке
        int i1 = sb.indexOf("<", 0);
        int i2 = sb.indexOf(">", i1 + 1);
        if (i1 < 0 || i2 <= i1) return false;
        
        // -- hash - это просто длина строки вместе с треугольными скобками, извлечём её
        int hi = sb.indexOf(";", i1);
        if (hi < 0) {
            sb.delete(0, i2 + 1);
            return false;
        }
        String hash = sb.substring(i1 + 1, hi);
        sb.delete(i1 + 1, hi + 1);
        String s = sb.substring(i1 + 1, i2 - hash.length() - 1);
        sb.delete(0, i2 - hash.length());
        try {
            // -- если длина не совпала - пакет битый
            if (s.length() != Integer.parseInt(hash)) return false;
        } catch (Exception e) { return false; }
        
        // -- теперь парсим идентификаторы и значения
        int i0 = 0;
        for (int i = 1; i < s.length(); ++i) {
            if ( !(s.charAt(i) == ';' && s.charAt(i - 1) != '\\') ) continue;
            String str = s.substring(i0, i);
            i0 = i + 1;
            int ind = -1;
            for (int j = 1; j < str.length(); ++j)
                if (str.charAt(j) == '=' && str.charAt(j - 1) != '\\') {
                    ind = j;
                    break;
                }
            if (ind < 0) continue;
            items.add(new Item(
                strDecode(str.substring(0, ind)),
                strDecode(str.substring(ind + 1))
            ));
        }
        return true;
    }
    
    /**
     * Закодировать строку, исключая вхождения спецсимволов
     */
    private String strEncode(String s) {
        StringBuilder sb = new StringBuilder();
        int index;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            index = -1;
            for (int j = 0; j < pspdlm.length(); j++)
                if (c == pspdlm.charAt(j)) {
                    index = j;
                    break;
                }
            if (index > -1)
                sb.append("\\" + index);
            else sb.append(c);
        }
        return sb.toString();
    }
    
    /**
     * Распаковка строки, закодированной функцией strEncode
     */
    private String strDecode(String s) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < s.length()) {
            char c = s.charAt(i);
            if (c == '\\') {
                if (i + 1 < s.length()) {
                    int k = Integer.parseInt(s.substring(i + 1, i + 2));
                    sb.append(pspdlm.charAt(k));
                }
                ++i;
            } else sb.append(c);
            ++i;
        }
        return sb.toString();
    }
    
    private final List<Item> items;
    private final String pspdlm = "\\<>;=";
    
    private static class Item {
        private final String id;
        private final int hash;
        private String value;
        
        public Item(String id, String value) {
            this.id = id;
            this.hash = id.hashCode();
            this.value = value;
        }
    }
    
}