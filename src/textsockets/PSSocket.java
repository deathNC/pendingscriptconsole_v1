/*
 * Neiro Technology 2011-2013
 */
package textsockets;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * Сокетная обёртка, служит для отправки/чтения пакетов PSPackage
 * @author deathNC
 */
public abstract class PSSocket {
    
    private Thread readingThread;
    private Thread sendingThread;
    
    public PSSocket(Socket client) {
        this.socket = client;
        sendList = new ArrayList<>();
        stopped = false;
        init();
    }
    
    private void init() {
        /* -- -- ПОТОК ЧТЕНИЯ ПАКЕТОВ -- -- */
        readingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                InputStream in = null;
                try {
                    in = socket.getInputStream();
                } catch (IOException e) { return; }
                byte[] buff = new byte[512];
                int errors = 0;
                StringBuilder sb = new StringBuilder();
                PSPackage pkg = new PSPackage();
                gencycle: while (!stopped) {
                    try {
                        int i = in.read(buff, 0, buff.length);
                        if (i == 0) {
                            Thread.sleep(75);
                            continue;
                        }
                        Thread.sleep(25);
                        while (i > 0) {
                            if (stopped) break gencycle;
                            String s1 = new String(buff, 0, i, "UTF8");
                            sb.append(s1);
                            if (in.available() > 1)
                                i = in.read(buff, 0, buff.length);
                            else i = 0;
                        }
                        //System.out.println("sb.> " + sb);
                        while (pkg.unpack(sb)) synchronized(PSSocket.this) { onPackage(pkg); }
                    } catch (SocketTimeoutException e) {
                        //debug("reading time_out");
                        continue;
                    } catch (Exception e) {
                        if (e instanceof IOException) ++errors;
                        debug("Exception: " + e);
                        if (errors > 5) {
                            debug("Отключение клиента через исключение!");
                            stopped = true;
                            break;
                        }
                    }
                }
                //synchronized (PSSocket.this) {
                onClosed(PSSocket.this.socket);
                //}
                debug("Отключение клиента!");
                //sendingThread.interrupt();
            }
        });
        readingThread.setPriority((Thread.MIN_PRIORITY + Thread.NORM_PRIORITY) / 2);
        readingThread.start();
        
        /* -- -- ПОТОК ОТПРАВКИ ПАКЕТОВ -- -- */
        sendingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                OutputStream out = null;
                try { out = socket.getOutputStream(); }
                catch (IOException e) { return; }
                while (!stopped) {
                    try {
                        PSPackage pkg = PSSocket.this.getNextPackage();
                        if (pkg == null) {
                            Thread.sleep(75);
                            continue;
                        }
                        String s = pkg.pack();
                        byte[] bt = s.getBytes("UTF8");
                        //System.out.println("bytes.len: " + bt.length + ", s.len: " + s.length());
                        
                        out.write(bt);
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        sendingThread.setPriority((Thread.MIN_PRIORITY + Thread.NORM_PRIORITY) / 2);
        sendingThread.start();
    }
    
    /**
     * Останавливает работу сокетной обёртки. Однако не завершает работу самого
     * соединения. Если требуется, это нужно сделать вручную.
     */
    public void stop() {
        stopped = true;
    }
    
    /**
     * Асинхронная отправка пакета, вызывающий поток не блокируется. Так как
     * для этого метода создана очередь, можно вызывать её немедленно сколько
     * угодно раз.
     * @param pkg отправляемый пакет
     */
    public void sendPackage(PSPackage pkg) {
        synchronized (sendList) {
            sendList.add(0, pkg);
        }
    }
    
    /**
     * Получение слудеющего пакета в очереди (не редактировать!)
     * @return null, если пакетов в очереди на отправку нет
     */
    private PSPackage getNextPackage() {
        synchronized (sendList) {
            if (sendList.size() > 0) return sendList.remove(sendList.size() - 1);
            else return null;
        }
    }
    
    
    /**
     * Событие возникает, когда из сети получен новый пакет
     * @param pkg пришедший пакет
     */
    public abstract void onPackage(PSPackage pkg);
    
    /**
     * Событие возникает при завершении работы потоков i/o. Например по причине
     * вызова метода stop()
     * @param socket это экземпляр сокета, может пригодиться для вызова в нём 
     * метода Close()
     */
    public abstract void onClosed(Socket socket);
    
    
    private void debug(Object o) {
        if (isDebugMode) System.out.println(o);
    }
    
    private volatile boolean stopped;
    private final boolean isDebugMode = true;
    private final Socket socket;
    private final List<PSPackage> sendList;
}
