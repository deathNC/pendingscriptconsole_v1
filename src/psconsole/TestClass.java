
package psconsole;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TestClass {

    public TestClass() {
        files = new LinkedList<>();
    }
    
    public void scanFolder(String folderPath) {
        if (folderPath.endsWith("__history")) return;
        File root = new File(folderPath);
        if (!root.exists()) return;
        File[] items = root.listFiles();
        String[] filters = {".java", ".pas", ".dpr"};
        for (int i = 0; i < items.length; ++i) {
            File file = items[i];
            if (file.isFile()) {
                boolean res = false;
                for (int j = 0; j < filters.length; ++j)
                    res = res | (file.getName().endsWith(filters[j]));
                if (res) files.add(file);
            } else if (file.isDirectory()) {
                scanFolder(file.getPath());
            }
        }
    }
    
    public void printCount() throws FileNotFoundException {
        int total = 0;
        long totalSize = 0l;
        String ln;
        for (File file : files) {
            Scanner scan;
            String encoding = "windows-1251";
            if (file.getName().endsWith(".java"))
                scan = new Scanner(file);
            else scan = new Scanner(file, encoding);
            int cnt = 0;
            try {
                while (scan.nextLine() != null) ++cnt;
            } catch (Exception e) {
            }
            total += cnt;
            totalSize += file.length();
            System.out.println(file.getName());
        }
        System.out.println("");
        System.out.println("Total: " + total + " lines, (" + String.format("%1.2f",
                (double)totalSize/1024D) + " KiB)");
    }
    
    public final List<File> files;
    
    public static void main(String[] args) throws Exception {
        TestClass test = new TestClass();
        
        test.scanFolder("C:/Users/Neiro User/Documents/NetBeansProjects/PendingScriptConsole/src/");
        test.scanFolder("F:/Projects/Delphi/Neiro Technology Projects/Soft/Pending Script Console (dll32)/");
        test.files.add(new File("C:/Program Files (x86)/Terminal Beta/experts/Pending Script Console DLL.mq4"));
        test.printCount();
    }
    
}