/*
 * Neiro Technology 2011-2014
 */
package psconsole;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import textsockets.PSPackage;
import textsockets.PSSocket;

/**
 * Сервер служебных процессов
 * @author deathNC
 */
public class ServerProcess {
    
    public ServerProcess(int port) throws IOException {
        serv = new ServerSocket(port);
        connection = null;
        // -- поиск соединений
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Socket sock = serv.accept();
                        ServerProcess.this.addConnection(sock);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
    
    private synchronized void addConnection(Socket socket) {
        this.connection = new ProcessConnection(socket);
    }
    
    public synchronized void startCProcessorForWindows() throws IOException {
        // -- загрзузка процесса из памяти
        
        // -- запуск процесса
        final Process process = Runtime.getRuntime().exec("F:/Projects/Delphi/"
                + "Neiro Technology Projects/Soft/Pending Script Console (dll32)/"
                + "ConsoleProcessor/cprocessor.exe");
    }
    
    public static class ProcessConnection extends PSSocket {
        
        public ProcessConnection(Socket client) {
            super(client);
        }
        
        @Override
        public void onPackage(PSPackage pkg) {
            String cmd = pkg.get("cmd");
            switch (cmd) {
                
                case (""):
                    
                    break;
                
                default:
                    System.out.println(" > ProcessConnection.onPackage: " + cmd);
                    break;
            }
        }
        
        @Override
        public void onClosed(Socket socket) {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    private final ServerSocket serv;
    private ProcessConnection connection;
}
