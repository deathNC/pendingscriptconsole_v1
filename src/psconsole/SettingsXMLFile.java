/*
 * Neiro Technology 2011-2014
 */
package psconsole;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Кэш настроек, сохраняемых в xml-файле. Поддержка сохранения с периодом
 * (для асинхронных систем).<br>
 * Так же класс имеет статический метод получения котировок Центробанка РФ.
 * @author deathNC
 */
public class SettingsXMLFile implements Runnable {
        
    public static void main(String[] args) {
        
    }
    
    
    /**
     * Загрузка xml из файла
     * @param fileName
     * @return null, если произошла ошибка, иначе - указатель на коренной элемент
     */
    public static Element loadDocument(String fileName) {
        File fl = new File(fileName);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(fl);
            return doc.getDocumentElement();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            return null;
        }
        
        
    }
    
    /**
     * Загрузка xml-документа из файла или, если файл не найден, выделение
     * памяти под новый документ.
     * @param fileName имя файла с настройками
     * @throws Exception может возникнуть при генерации xml или при чтении
     * из файла. В случае генерации Exception следует завершить работу
     * приложения.
     */
    public SettingsXMLFile(String fileName, String rootItemName) throws Exception {
        this.fileName = fileName;
        stopped = false;
        // -- создание директории для файла
        File fl = new File(fileName);
        File par = new File(fl.getParent());
        if (!par.exists()) par.mkdirs();
        // -- загрузка документа или создание нового
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        if (fl.exists()) doc = db.parse(fl);
        else {
            doc = db.newDocument();
            Element root = doc.createElement(rootItemName);
            doc.appendChild(root);
        }
        // -- запуск сохраняющего потока
        Thread t = new Thread(this);
        t.setPriority(Thread.MIN_PRIORITY + 1);
        t.start();
    }
    
    @Override
    public void run() {
        long lastTime = 0;
        while (!stopped) {
            try {
                Thread.sleep(SLEEP_TIME);
                if (!canSave) continue;
                if (System.currentTimeMillis() - lastTime < SLEEP_TIME * 3) continue;
                save();
                canSave = false;
                lastTime = System.currentTimeMillis();
            } catch (Exception e) {}
        }
    }
    
    public void stop() {
        canSave = false;
        stopped = true;
        save();
    }
    
    /**
     * Возвращает актуальную версию документа, которая постоянно висит в кэше.
     * @return null, если в конструкторе произошла ошибка
     */
    public synchronized Document getDocument() {
        return doc;
    }
    
    /**
     * Сохранение настроек
     * @param now если true, то настройки сохранятся немедленно
     */
    public synchronized void saveSettings(boolean now) {
        if (now) {
            save();
        } else {
            canSave = true;
        }
    }
    
    private synchronized void save() {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            t.setOutputProperty(OutputKeys.METHOD, "xml");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            FileOutputStream fout = new FileOutputStream(new File(fileName));
            t.transform(new DOMSource(doc), new StreamResult(fout));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    private static String getChild(Element parent, String name) {
        NodeList nl = parent.getElementsByTagName(name);
        if (nl.getLength() > 0) {
            return ((Element)nl.item(0)).getTextContent();
        }
        else return null;
    }
    
    public static List<CurrencyItemCBRF> getCurrencyCBRF() {
        List<CurrencyItemCBRF> list = new LinkedList<>();
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp?");
            URLConnection conn = url.openConnection();
            Document doc = db.parse(conn.getInputStream());
            Element root = doc.getDocumentElement();
            //System.out.println("doc.root = " + root.getNodeName() + " = " + root.getNodeValue());
            NodeList nl = root.getElementsByTagName("Valute");
            for (int i = 0; i < nl.getLength(); ++i) {
                try {
                    Element item = (Element)nl.item(i);
                    if (!item.hasChildNodes()) continue;
                    String id = item.getAttribute("ID");
                    int numCode = Integer.parseInt(getChild(item, "NumCode"));
                    String charCode = getChild(item, "CharCode");
                    double nominal = Double.parseDouble(getChild(item, "Nominal"));
                    String name = getChild(item, "Name");
                    double value = Double.parseDouble(getChild(item, "Value").replace(',', '.'));
                    list.add(new CurrencyItemCBRF(id, numCode, charCode, nominal, name, value));
                    //System.out.println(" <" + name + ">");
                } catch (Exception e) { 
                    e.printStackTrace();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Элемент списка валютных стоимостей в российских рублях.
     * Данные сервера Центробанка Российской Федерации
     * @author deathNC
     */
    public static class CurrencyItemCBRF {

        public CurrencyItemCBRF(String id, int numCode, String charCode,
                double nominal, String name, double value) {
            this.numCode = numCode;
            this.charCode = charCode;
            this.nominal = nominal;
            this.name = name;
            this.value = value;
            this.id = id;
        }
        
        public static double getCurrencyValue(String charCode, List<CurrencyItemCBRF> list) {
            double res = 0.0;
            for (CurrencyItemCBRF item : list) {
                if (!item.charCode.equals(charCode)) continue;
                res = item.value / item.nominal;
                break;
            }
            return res;
        }
        
        public final String id;
        public final int numCode;
        public final String charCode;
        public final double nominal;
        public final String name;
        public final double value;
    }
    
    
    
    private final Document doc;
    
    private final String fileName;
    private volatile boolean stopped;
    private volatile boolean canSave;
    private final long SLEEP_TIME = 250;
}
