/*
 * Neiro Technology 2011-2014
 */
package psconsole;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import textsockets.PSPackage;

/**
 * Список ордеров и определение типов ордеров.<br>
 * Предназначен только для <b>MetaTrader 4</b>
 * @author deathNC
 */
public class OrderList {
    
    public OrderList() {
        orders = new ArrayList<>();
    }
    
    public OrderItem get(int index) {
        return orders.get(index);
    }
    
    public OrderItem getByID(long id) {
        for (OrderItem o : orders)
            if (o.id == id) return o;
        return null;
    }
    
    /**
     * Возвращает ордер с наибольшим значением ID.
     * @return null, если список пуст
     */
    public OrderItem getLastByID() {
        OrderItem order = null;
        for (OrderItem i : orders) {
            if (order == null) order = i;
            else if (i.id > order.id) order = i;
        }
        return order;
    }
    
    public int count() {
        return orders.size();
    }
    
    /**
     * Обновление списка ордеров. Если объект данного класса используется для
     * исторических ордеров, то надо юзать [isReplace = true].
     * @param pkg пакет, который содержит список ордеров и свойство size
     * @param isReplace если true, то метод сработает в режиме замены. Значит,
     * что те ордеры списка, которых не оказалось в пришедшем пакете, будут
     * удалены из списка.
     */
    public void refreshOrders(PSPackage pkg, boolean isReplace) {
        try {
            // -- обнуление флага
            for (OrderItem o : orders) o.flag = false;
            
            int i1 = 0, i2 = 0;
            Properties props = new Properties();
            int n = Integer.parseInt(pkg.get("size"));
            for (int i = 0; i < n; ++i) {
                String s = pkg.get("o-" + i);
                String comm = "";
                // -- парсинг строки на отдельные элементы (свойства ордера)
                int buff = s.indexOf(";comment=");
                if (buff > -1) {
                    comm = s.substring(buff + ";comment=".length());
                    s = s.substring(0, buff);
                }
                // -- парсим параметры
                //System.out.println(" >> " + s);
                StringTokenizer st = new StringTokenizer(s, ";");
                props.clear();
                String bf;
                int ind;
                while (st.hasMoreTokens()) {
                    bf = st.nextToken();
                    ind = bf.indexOf('=');
                    String name = bf.substring(0, ind);
                    String vl = bf.substring(ind + 1);
                    props.setProperty(name, vl);
                    //System.out.println("  >>  [" + name + "] = [" + vl + "]");
                }
                // -- обновляем/добавляем ордер
                long id = Long.parseLong(props.getProperty("id", "-1"));
                if (id == -1) continue;
                OrderItem order = getByID(id);
                if (order == null) {
                    order = new OrderItem(id, props.getProperty("smb"));
                    orders.add(order);
                    ++i2;
                }
                order.type = OrderType.valueOf(Integer.parseInt(props.getProperty("type")));
                order.timeOpen = Long.parseLong(props.getProperty("tOpen")) * 1000;
                order.profit = Double.parseDouble(props.getProperty("profit"));
                if (order.type.code >= OrderType.OP_BUY.code && order.type.code <= OrderType.OP_SELLSTOP.code) {
                    order.priceOpen = Double.parseDouble(props.getProperty("pOpen"));
                    order.priceClose = Double.parseDouble(props.getProperty("pClose"));
                    order.timeClose = Long.parseLong(props.getProperty("tClose")) * 1000;
                    order.lots = Double.parseDouble(props.getProperty("lots"));
                    order.stoploss = Double.parseDouble(props.getProperty("sl"));
                    order.takeprofit = Double.parseDouble(props.getProperty("tp"));
                }
                order.comment = comm;
                // -- отмечаем ордер как существующий
                order.flag = true;
                ++i1;
            }
            
            // -- удаление ненужных ордеров ()
            if (isReplace) {
                Iterator<OrderItem> i = orders.iterator();
                while (i.hasNext())
                    if (!i.next().flag) i.remove();
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
    
    public void exportIntoNode(Element node) {
        Document doc = node.getOwnerDocument();
        // -- зачистка нода
        NodeList nl = node.getChildNodes();
        for (int i = nl.getLength() - 1; i >= 0; ++i)
            node.removeChild(nl.item(i));
        // -- сохранение списка
        for (OrderItem i : orders) {
            Element item = doc.createElement("order");
            i.exportIntoNode(item);
            node.appendChild(item);
        }
    }
    
    public void importFromNode(Element node) {
        orders.clear();
        NodeList nl = node.getElementsByTagName("order");
        for (int i = 0; i < nl.getLength(); ++i) {
            Element o = (Element)nl.item(i);
            try {
                long id = Long.parseLong(o.getAttribute("id"));
                String smb = o.getAttribute("smb");
                OrderItem order = new OrderItem(id, smb);
                order.type = OrderType.valueOf(Integer.parseInt(o.getAttribute("type")));
                order.timeOpen = Long.parseLong(o.getAttribute("tmOpen"));
                order.profit = Double.parseDouble(o.getAttribute("profit"));
                if (order.type.code >= 0 && order.type.code < 6) {
                    order.lots = Double.parseDouble(o.getAttribute("amount"));
                    order.priceOpen = Double.parseDouble(o.getAttribute("priceOpen"));
                    order.priceClose = Double.parseDouble(o.getAttribute("priceClose"));
                    order.timeClose = Long.parseLong(o.getAttribute("tmClose"));
                    order.stoploss = Double.parseDouble(o.getAttribute("sl"));
                    order.takeprofit = Double.parseDouble(o.getAttribute("tp"));
                }
                order.comment = o.getAttribute("comment");
                orders.add(order);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private final List<OrderItem> orders;
    
    public static class OrderItem<K> {
        
        public OrderItem(long id, String symbol) {
            resetOrderData();
            this.id = id;
            this.symbol = symbol;
            this.data = null;
            this.type = OrderType.OP_BUY;
            this.trailStart = false;
        }
        
        public void resetOrderData() {
            this.priceOpen = 0.0;
            this.timeOpen = 0;
            this.priceClose = 0.0;
            this.timeClose = 0;
            this.stoploss = 0.0;
            this.takeprofit = 0.0;
            this.trailing = 0.0;
            this.lots = 0.0;
            this.profit = 0.0;
        }
        
        /**
         * Является ли ордер покупкой (OP_BUY | OP_BUYLIMIT | OP_BUYSTOP)
         * @return true, если этот ордер создан на покупку
         */
        public boolean isLong() {
            return (type == OrderType.OP_BUY) ||
                   (type == OrderType.OP_BUYLIMIT) ||
                   (type == OrderType.OP_BUYSTOP);
        }
        
        /**
         * Является ли ордер продажей (OP_SELL | OP_SELLSTOP | OP_SELLLIMIT)
         * @return true, если этот ордер создан на продажу
         */
        public boolean isShort() {
            return (type == OrderType.OP_SELL) ||
                   (type == OrderType.OP_SELLLIMIT) ||
                   (type == OrderType.OP_SELLSTOP);
        }
        
        @Override
        public String toString() {
            return Long.toString(id);
        }
        
        /**
         * Экспорт ордера в элемент дерева xml. Данные, нужные только в режиме
         * run-time (TrailingStop и т.п.) не экспортируются!
         * @param o элемент, в атрибуты которого будут записаны все данные
         */
        public void exportIntoNode(Element o) {
            o.setAttribute("id", Long.toString(id));
            o.setAttribute("smb", symbol);
            o.setAttribute("type", Integer.toString(type.code));
            o.setAttribute("tmOpen", Long.toString(timeOpen));
            o.setAttribute("profit", Double.toString(profit));
            if (type.code >= 0 && type.code < 6) {
                o.setAttribute("amount", Double.toString(lots));
                o.setAttribute("priceOpen", Double.toString(priceOpen));
                o.setAttribute("priceClose", Double.toString(priceClose));
                o.setAttribute("tmClose", Long.toString(timeClose));
                o.setAttribute("sl", Double.toString(stoploss));
                o.setAttribute("tp", Double.toString(takeprofit));
            }
            o.setAttribute("comment", comment);
        }
        
        //<editor-fold desc=" terminal data ">
        
        /**
         * Идентификатор ордера в торговой системе.<br>
         * Например, в MT4 - это OrderTicket()
         */
        public final long id;
        /**
         * Валютная пара, в которой выполнен ордер
         */
        public final String symbol;
        /**
         * Тип ордера
         */
        public OrderType type;
        /**
         * Объём операции (пока что в Lots от MT4)
         */
        public double lots;
        /**
         * Цена открытия ордера
         */
        public double priceOpen;
        /**
         * Время открытия ордера.
         */
        public long timeOpen;
        /**
         * Уровень StopLoss. Указывается рыночной ценой, НЕ пунктами!
         */
        public double stoploss;
        /**
         * Уровень TakeProfit. Указывается рыночной ценой, НЕ пунктами!
         */
        public double takeprofit;
        /**
         * Цена закрытия ордера
         */
        public double priceClose;
        /**
         * Время закрытия ордера
         */
        public long timeClose;
        /**
         * Прибыль ордера в базовой валюте счёта
         */
        public double profit;
        /**
         * Комментарий ордера в том виде, в каком он есть в торговом терминале
         */
        public String comment;

        /**
         * Трейлинг-стоп, указывается в пунктах
         */
        public boolean trailStart;
        /**
         * Показывает, нужен ли начальный трейлинг (с минимальным stoploss)
         */
        public double trailing;
        /**
         * Используется на усмотрение программиста.
         * На данный момент юзается для определения - есть ли ордер в новом
         * списке.
         */
        public boolean flag;
        
        //</editor-fold>
        
        public K data;
    }
    
    /**
     * Показывает тип рыночного ордера
     */
    public enum OrderType {
        /**
         * Ордер на покупку основной валюты в паре
         */
        OP_BUY          ("buy",         0),
        /**
         * Ордер на продажу основной валюты в паре
         */
        OP_SELL         ("sell",        1),
        /**
         * Отложенный ордер на покупку основной валюты в паре. Выставляется
         * <b>ниже</b> текущей рыночной цены.
         */
        OP_BUYLIMIT     ("buy limit",   2),
        /**
         * Отложенный ордер на продажу основной валюты в паре. Выставляется
         * <b>выше</b> текущей рыночной цены.
         */
        OP_SELLLIMIT    ("sell limit",  3),
        /**
         * Отложенный ордер на покупку основной валюты в паре. Выставляется
         * <b>выше</b> текущей рыночной цены.
         */
        OP_BUYSTOP      ("buy stop",    4),
        /**
         *  Отложенный ордер на продажу основной валюты в паре. Выставляется
         * <b>ниже</b> текущей рыночной цены.
         */
        OP_SELLSTOP     ("sell stop",   5),
        /**
         * Операция пополнения счёта или вывода средств из него. Пригодится
         * для генерации статистики на всех расчётных периодах данного торгового
         * счёта.<br>
         * Такой ордер может быть только в истории. С него можно получить данные:
         * <b>OrderProfit()</b> - это количество введённых/выведеных средств,<br>
         * <b>OrderTicket()</b> - идентификатор операции,<br>
         * <b>OrderOpenTime()</b> - точная дата операции,<br>
         * <b>OrderComment()</b> - показывает идентификатор аккаунта (может
         * отличаться от торгового логина) + показывает тип операции (ввод/вывод).
         * Либо может быть указана причина пополнения/снятия средств. Не стоит
         * использовать эту инфу для идентификации чего либо.
         */
        OP_BALANCE      ("balance",     6),
        /**
         * Денежный бонус от брокера. Довольно редкое (ненужное) явление при
         * нормальном трейдинге, однако, случается.
         */
        OP_CREDIT       ("credit",      7),
        /**
         * Неопознанный тип ордера. 
         */
        OP_UNKNOWN      ("unknown",     64);
        
        public final String description;
        public final int code;
        
        private OrderType(String description, int code) {
            this.code = code;
            this.description = description;
        }
        
        @Override
        public String toString() {
            return description;
        }
        
        /**
         * Возвращает указатель на элемент перечисления по его коду
         * @param code int-значение элемента (или его индекс)
         * @return null, если такого элемента не существует
         */
        public static OrderType valueOf(int code) {
            if      (code == 0) return OP_BUY;
            else if (code == 1) return OP_SELL;
            else if (code == 2) return OP_BUYLIMIT;
            else if (code == 3) return OP_SELLLIMIT;
            else if (code == 4) return OP_BUYSTOP;
            else if (code == 5) return OP_SELLSTOP;
            else if (code == 6) return OP_BALANCE;
            else if (code == 7) return OP_CREDIT;
            else return OP_UNKNOWN;
        }
    }
}
