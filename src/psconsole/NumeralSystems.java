/*
 * Neiro Technology 2011-2013
 */
package psconsole;

import java.util.Random;

/**
 * Класс для генерации сжатых идентификаторов. Конвертирует long-числа в систему
 * счисления по основания 62.
 * @author deathNC
 */
public class NumeralSystems {
    
    private final Random rand = new Random();
    private final String digit = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    public String packDecimal(long value) {
        int digSize = digit.length();
        long x = value;
        StringBuilder s = new StringBuilder();
        while (Math.abs(x) > 0) {
            long d = x % digSize;
            s.insert(0, digit.charAt((int)d));
            x = x / digSize;
        }
        if (s.length() == 0) return "0";
        else return s.toString();
    }
    
    /**
     * Генерация случайного числа
     * @return
     */
    public String geterateID() {
        StringBuilder sb = new StringBuilder(packDecimal(System.currentTimeMillis() / 1000));
        sb.append(Integer.toHexString(rand.nextInt(255)));
        while (sb.length() < 8)
            sb.insert(0, '0');
        return sb.toString();
    }
    
    public static void main(String[] args) throws Exception {
        NumeralSystems ns = new NumeralSystems();
        System.out.println(" > " +
                ns.packDecimal(916224000L + System.currentTimeMillis() / 1000) + 
                Integer.toHexString(255));
    }
}
