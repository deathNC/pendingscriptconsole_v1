/*
 * Neiro Technology 2011-2013
 */

package psconsole;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Коллекция пользовательских торговых символов
 * @author deathNC
 */
public class SymbolList {

    public SymbolList(DefaultListModel<Symbol> list) {
        this.model = list;
    }
    
    /**
     * Мягкий поиск символа в списке
     * @param s имя символа целиком или отрывок имени
     * @return null, если ни одного совпадения не найдено
     */
    public Symbol search(String s) {
        if (s == null || s.isEmpty()) return null;
        s = s.toLowerCase();
        for (int i = 0; i < model.getSize(); ++i) {
            Symbol smb = model.elementAt(i);
            String bf = smb.symbol.toLowerCase();
            if (bf.indexOf(s) != 0 || bf.length() < s.length()) continue;
            return smb;
        }
        return null;
    }
    
    public Symbol search(String s, boolean fullnameOnly) {
        if (!fullnameOnly) return search(s);
        else {
            if (s == null || s.isEmpty()) return null;
            s = s.toLowerCase();
            for (int i = 0; i < model.getSize(); ++i) {
                Symbol smb = model.elementAt(i);
                String bf = smb.symbol.toLowerCase();
                if (bf.equals(s))
                return smb;
            }
        }
        return null;
    }
    
    /**
     * Добавляет новый символ в список
     * @param symbol имя символа
     * @return созданный символ
     */
    public Symbol add(String symbol) {
        Symbol smb = new Symbol(symbol);
        model.addElement(smb);
        return smb;
    }
    
    /**
     * Удаляет символ из списка
     * @param symbol указатель на символ
     */
    public void remove(Symbol symbol) {
        for (int i = 0; i < model.getSize(); ++i)
            if (model.get(i) == symbol) {
                model.remove(i);
                break;
            }
    }
    
    /**
     * Удаление всех элементов
     */
    public void clear() {
        model.removeAllElements();
    }
    
    /**
     * Возвращает список включённых валютных пар, разделённых запятыми
     * @return пустая строка (не null), если список пуст
     */
    public String getSymbolList() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < model.getSize(); ++i) {
            sb.append(model.get(i).symbol.toUpperCase());
            if (i <= model.getSize() - 1)
                sb.append(',');
        }
        return sb.toString();
    }
    
    /**
     * Экспорт списка символов и их параметров в структуру XML
     * @param root корневой элемент, в котором создавать ноды с символами
     */
    public void exportToXML(Element root) {
        Document doc = root.getOwnerDocument();
        for (int i = 0; i < model.getSize(); ++i) {
            Symbol x = model.get(i);
            Element item = doc.createElement("item");
            item.setAttribute("symbol", x.symbol);
            item.setAttribute("digits", Integer.toString(x.digits));
            item.setAttribute("stoplevel", Double.toString(x.minSL));
            item.setAttribute("bid", String.format(Locale.US, "%1." + x.digits + "f", x.bid));
            item.setAttribute("ask", String.format(Locale.US, "%1." + x.digits + "f", x.ask));
            root.appendChild(item);
        }
    }
    
    /**
     * Импорт списка символов из структуры xml
     * @param root корень
     */
    public void importFromXML(Element root) {
        clear();
        NodeList nl = root.getElementsByTagName("item");
        for (int i = 0; i < nl.getLength(); ++i) {
            Element item = (Element)nl.item(i);
            Symbol smb = new Symbol(item.getAttribute("symbol"));
            try {
                smb.digits = Integer.parseInt(item.getAttribute("digits"));
                smb.minSL = Double.parseDouble(item.getAttribute("stoplevel"));
                smb.bid = Double.parseDouble(item.getAttribute("bid"));
                smb.ask = Double.parseDouble(item.getAttribute("ask"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            model.addElement(smb);
        }
    }
    
    public void refreshSymbolData(String data) {
        StringTokenizer arr = new StringTokenizer(data, ";");
        ArrayList<String> items = new ArrayList<>();
        while (arr.hasMoreTokens()) {
            String s = arr.nextToken();
            StringTokenizer st = new StringTokenizer(s, ",");
            items.clear();
            while (st.hasMoreTokens()) items.add(st.nextToken());
            if (items.size() != 5) continue;
            Symbol smb = this.search(items.get(0), true);
            if (smb == null) continue;
            try {
                smb.bid = Double.parseDouble(items.get(1));
                smb.ask = Double.parseDouble(items.get(2));
                smb.digits = Integer.parseInt(items.get(3));
                smb.minSL = Double.parseDouble(items.get(4));
            } catch (Exception e) { e.printStackTrace(); }
        }
    }
    
    private final DefaultListModel<Symbol> model; // lstSymbolList model
    
    
    /**
     * Описание одного объекта-символа. Содержит необходимые для торговли
     * данные (история котировок, минимальный SL и т.д.).
     */
    public static class Symbol {
        
        public Symbol(String symbol) {
            this.symbol = symbol;
            this.ticks = new Ticks();
            minSL = 0;
            digits = 0;
        }
        
        @Override
        public String toString() {
            return this.symbol;
        }
        
        /**
         * Последняя цена Bid по данной валютной паре
         */
        public double bid;
        /**
         * Последняя цена Ask по данной валютной паре
         */
        public double ask;
        /**
         * Количество цифр после запятой
         */
        public int digits;
        /**
         * Минимальный уровень stoploss'а
         */
        public double minSL;
        /**
         * Валютная пара, инструмент
         */
        public final String symbol;
        
        /**
         * Коллцекция исторических тиков (пока не реализовано)
         */
        public final Ticks ticks;
    }
    
    /**
     * Обеспечивает хранение истории котировок (цены bid/ask).
     */
    public static class Ticks {
        
    }
}
