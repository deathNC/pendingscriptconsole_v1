/*
 * Neiro Technology 2011-2014
 */
package psconsole;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import psconsole.OrderList.OrderItem;
import psconsole.OrderList.OrderType;

/**
 * Элемент списка терминалов
 * @author deathNC
 */
public class UserItemRenderer extends javax.swing.JPanel {
    
    /**
     * Creates new form UserItemRenderer
     */
    public UserItemRenderer() {
        initComponents();
    }
    
    public void setItemSelected(boolean selected) {
        setBackground(selected ? new Color(230, 230, 230) : Color.white);
    }
    
    public void setValue(UserItemValue value, MainForm form) {
        lblAccountID.setText(value.accountID != -1 ?
                Long.toString(value.accountID) :  "[" + value.terminalCount + "]");
        // name
        if (value.accountID != -1) {
            StringTokenizer st = new StringTokenizer(value.name, " ");
            StringBuilder sb = new StringBuilder();
            sb.append(st.nextToken());
            while (st.hasMoreTokens()) {
                sb.append(' ');
                sb.append(st.nextToken().toUpperCase().charAt(0));
                sb.append('.');
            }
            lblName.setText(sb.toString());
        } else lblName.setText(value.name);
        
        lblBalance.setText(form.getCurrencyStr(value.balance) + "$");
        lblProfit.setText(form.getCurrencyStr(value.profit) + "$");
        if (value.profit < -1.0) lblProfit.setForeground(profitRed);
        else if (value.profit > 1.0) {
            lblProfit.setForeground(profitGreen);
            lblProfit.setText("+" + lblProfit.getText());
        } else lblProfit.setForeground(profitNone);
    }
    
    public void setIconName(ImageIcon icon) {
        lblIcon.setIcon(icon);
    }
    
    /**
     * Класс описывает один торговый счёт (в связке с торговым терминалом)
     */
    public static class UserItemValue {
        
        private void initClass() {
            terminalCount = 0;
            terminalConnected = false;
            traderComission = 50;
            currency = "USD";
            terminalAddress = "";
            lastHistID = 0;
            lastBalance = 0.0;
        }
        
        public UserItemValue(long accID) {
            this.accountID = accID;
            this.name = "User<?>";
            this.balance = 0.0;
            this.profit = 0.0;
            this.orders = new OrderList();
            this.history = new OrderList();
            initClass();
        }
        
        public UserItemValue(long accID, String name, double balance, double profit) {
            this.accountID = accID;
            this.name = name;
            this.balance = balance;
            this.profit = profit;
            this.orders = new OrderList();
            this.history = new OrderList();
            initClass();
        }
        
        /**
         * Сохраняет аккаунт в xml-node. Так же вызывает сохранение истории.
         * @param node корневой элеметн списка аккаунтов
         */
        public void xmlExportToNode(Element node) {
            Document doc = node.getOwnerDocument();
            Element user = doc.createElement("user");
            node.appendChild(user);
            // -- данные аккаунта и его терминала
            user.setAttribute("name", name);
            user.setAttribute("id", Long.toString(accountID));
            user.setAttribute("balance", Double.toString(balance));
            user.setAttribute("profit", Double.toString(profit));
            user.setAttribute("exe", terminalAddress);
            user.setAttribute("comission", Byte.toString(traderComission));
            user.setAttribute("currency", currency);
            // -- ордера
            Element item = doc.createElement("orders");
            user.appendChild(item);
            orders.exportIntoNode(item);
            // -- история
            saveOrdersHistory();
        }
        
        /**
         * Загрузка данных аккаунта с элемента xml
         * @param node xml-элемент, описывающий конкретного юзера
         */
        public void xmlImportFromNode(Element node) {
            try {
                name = node.getAttribute("name");
                terminalAddress = node.getAttribute("exe");
                balance = Double.parseDouble(node.getAttribute("balance"));
                profit = Double.parseDouble(node.getAttribute("profit"));
                traderComission = Byte.parseByte(node.getAttribute("comission"));
                String s = currency = node.getAttribute("currency");
                if (!s.equals(node.getAttribute("bazuza"))) currency = s;
                // загрузка ордеров:
                NodeList nl = node.getElementsByTagName("orders");
                if (nl.getLength() == 1)
                    orders.importFromNode((Element)nl.item(0));
                // загрузка истории
                loadOrdersHistory();
            } catch (Exception e) { e.printStackTrace(); }
        }
        
        /**
         * Сохранение истории ордеров в xml с именем {номер_аккаунта}.xml
         */
        public void saveOrdersHistory() {
            File fl = new File(MainForm.getOrdersHistoryDir() + "/" + Long.toString(accountID) + ".xml");
            try {
                if (fl.exists()) fl.delete();
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer t = tf.newTransformer();
                
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.newDocument();
                
                Element root = doc.createElement("OrdersHistory");
                doc.appendChild(root);
                
                history.exportIntoNode(root);
                
                t.setOutputProperty(OutputKeys.METHOD, "xml");
                t.setOutputProperty(OutputKeys.INDENT, "yes");
                FileOutputStream fout = new FileOutputStream(fl);
                t.transform(new DOMSource(doc), new StreamResult(fout));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        /**
         * Загрузка истории ордеров из xml с именем {номер_аккаунта}.xml
         */
        public void loadOrdersHistory() {
            File fl = new File(MainForm.getOrdersHistoryDir() + "/" + Long.toString(accountID) + ".xml");
            if (!fl.exists()) return;
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(fl);
                Element root = doc.getDocumentElement();
                history.importFromNode(root);
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        }
        
        
        /**
         * Возвращает последний в истории скорректированный баланс
         * @return null, если коррекции никогда не было
         */
        public OrderItem getLastBalanceCorrect() {
            for (int i = history.count() - 1; i >= 0; --i)
                if (history.get(i).type.equals(OrderType.OP_BALANCE))
                    return history.get(i);
            return null;
        }
        
        /**
         * Возвращает последний коррекционный баланс
         * @return возможен неточный результат из-за использования double
         * для подсчётов
         */
        public double getInitialBalance() {
            // -- оптимизация
            if ((history.count() > 0) && (history.get(history.count() - 1).id == lastHistID))
                return lastBalance;
            // -- если оптимизация не проканала, вычисляем
            double prf = 0.0;
            for (int i = history.count() - 1; i >= 0; --i) {
                if (history.get(i).type.code < 2) prf += history.get(i).profit;
                if (history.get(i).type.equals(OrderType.OP_BALANCE))
                    break;
            }
            lastHistID = history.count() == 0 ? 0 : history.get(history.count() - 1).id;
            lastBalance = balance - prf;
            return lastBalance;
        }
        
        /**
         * Возвращает прибыль с последней коррекции 
         * @return возможен неточный результат из-за использования double
         * для подсчётов
         */
        public double getProfit() {
            return balance - getInitialBalance();
        }
        
        // -- используется для оптимизации вычислений баланса
        private long lastHistID;
        private double lastBalance;
        
        
        //<editor-fold desc=" fields ">
        
        /**
         * Имя клиента, на которого оформлен счёт
         */
        public String name;
        /**
         * Номер счёта (идентификатор)
         */
        public final long accountID;
        /**
         * Текущий баланс счёта
         */
        public double balance;
        /**
         * Профит по открытым позициям
         */
        public double profit;
        
        /**
         * Список открытых ордеров
         */
        public final OrderList orders;
        /**
         * История ордеров данного счёта
         */
        public final OrderList history;
        
        /**
         * Комиссия трейдера в процентах (от 10% до 90%)
         */
        public byte traderComission;
        /**
         * Базовая валюта счёта
         */
        public String currency;
        
        /**
         * Путь к исполняемому файлу терминала
         */
        public String terminalAddress;
        /**
         * Кол-во терминалов (только для системного элемента)
         */
        public int terminalCount;
        /**
         * Показывает, подключен ли терминал
         */
        public boolean terminalConnected;
        
        //</editor-fold>
        
    }
    
    
    private final Color profitGreen = new Color(0, 128, 0);
    private final Color profitRed = new Color(128, 0, 0);
    private final Color profitNone = new Color(128, 128, 128);
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblName = new javax.swing.JLabel();
        lblAccountID = new javax.swing.JLabel();
        lblBalance = new javax.swing.JLabel();
        lblProfit = new javax.swing.JLabel();
        lblIcon = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(170, 42));

        lblName.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblName.setForeground(new java.awt.Color(51, 51, 51));
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblName.setText("Ораклянов К.И.");

        lblAccountID.setForeground(new java.awt.Color(102, 102, 102));
        lblAccountID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblAccountID.setText("18051");

        lblBalance.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblBalance.setText("6196.31$");

        lblProfit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblProfit.setForeground(new java.awt.Color(0, 153, 0));
        lblProfit.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblProfit.setText("+103.95$");

        lblIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/psconsole/resources/userActive2.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(lblIcon)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblBalance)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                        .addComponent(lblProfit))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(lblAccountID)))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(lblAccountID)
                    .addComponent(lblIcon))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBalance)
                    .addComponent(lblProfit))
                .addContainerGap(13, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblAccountID;
    private javax.swing.JLabel lblBalance;
    private javax.swing.JLabel lblIcon;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblProfit;
    // End of variables declaration//GEN-END:variables
}
